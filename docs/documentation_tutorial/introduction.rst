==============
 Introduction
==============

This document details the basic methods of editing any of our public facing documentation. It also serves as a reference for the syntax required and the version control system behind it.

In this manual we will cover:
	1. Registering a BitBucket account
	2. Installing Sourcetree on the local machine
	3. Cloning the documentation to the local machine
	4. Setting up Sublime Text 3 as the editing platform for the documentation
	5. Writing basic text with the reStructureText system
	6. Creating manuals using the Sphinx documentation system
	7. Pushing changes to the version control system and requesting integration with the original documentation set

.. hint:: It is highly advised that you use this manual in conjunction with attending a Basic Documentation Editing class, as an instructor will be available to assist with any installation issues.

.. warning:: This manual involves installing software. Please be careful as to where you are obtaining software from to avoid malware. 

