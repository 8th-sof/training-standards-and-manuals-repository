===================================
 Phase I - Q Course (Fundamentals)
===================================

Introduction:
=======

The Nature of Special operations is further defined by the Soldiers, Sailors, Airmen, and Marines who conduct them. SOF are carefully selected for their physical excellence, maturity, judgment, adaptability, and ability to make good decisions under pressure. SOF are trained in language and culture, special tactics, techniques, and procedures. they are provided with the most advanced equipment designed or modified for use within the special operations community.

As the result of their selection, training, equipment, and cultural attunement, SOF are able to operate in small teams in friendly, politically sensitive, uncertain, or hostile environments to achieve objectives independently or with or through indigenous forces and populations. SOF are able to conduct a wide range of missions, often high risk, and in a clandestine or low visibility mode when required.

Welcome to PHASE 1/TIER 1 of 8th SOF Training facility. We ask that you pay attention, maintain your professionalism, and keep your bearing. Listen and learn all you can, the information you hear will be invaluable to your success throughout your development.

The Rules:
==========

1. SAFETY Comes before all else
2. Listen to the instructions that are given to you by the Instructors
3. Do what you are told
4. Maintain focus on the task at hand.
5. Complete the objectives that are given before you. If you don't get it right, you will be given another opportunity to complete the task again.
6. Integrity goes a long way - if your caught cheating do not argue (You are being judged on your character, performance under stress). Accept the fault and move on...

Outline:
=========
Q-Course Course Starts at 00:00:00 where trainee(s) are trucked to undisclosed location. Where the Instructors, and Observers will take over. Trainee's will have NO MAP, only uniform/compass, radio, no GPS, and NO Weapon.

The Chain of Command (CoC) during training is as follows:
Field Evaluator: In charge of Drill Instructors, Responsible for the direction of training, keeping over all scores and conducting oversight on ongoing training.
Drill Instructors: Will conduct the Training, provide instruction, and assist in demonstration, supervise and insure SAFETY at all times.
Operators: Assist Trainee's and are the first line of instruction from the DI's. They are responsible to assist Instructors, assist Trainee's in completing tasks, and provide actual insight on ARMA tactics while maintaining SAFETY at all times.

Welcome to Field Orientation: Phase I/TERE 1 of training (Subjects to cover)

SUBJECT AREA 1: INDIVIDUAL CONDUCT AND LAWS OF WAR
---------------------------------------------------

TASK:
`````
Comply with regulations of conduct and the Rules of Engagement as sited by 8th Special Operations Task Force (NATO)


CONDITION:
```````````
You are a member of 8th SOF(N), a tier 1 MILSIM Community using ARMA 3. As an operator, you must identify, understand, and comply with the regulations of conduct, including the Rules of Engagement. You must also identify any suspected or known violations of regulations and notify the Chain of command.


STANDARDS:
``````````
Identify, understand, and comply with 8th SOF(N) regulations on conduct and ROE. Identify problems or situations that violate the policies and take appropriate action (including notifying appropriate authorities) so that expedient action may be taken to correct the problem or situation.


PERFORMANCE STEPS:
``````````````````

1. Define 8th SOF(N) policies:
  A) Reg. 8R-001.01
  B) Reg. 8R-002.01
  C) Explain the understanding of personal responsibility for knowledge of future policies.

2. Define Rules of Engagement (ROE)
  A) Describe universal active ROE (UAROE)
  B) Describe the International Law's prohibition on intentionally targeting or attacking civilians.
  C) Describe when attacking a military objective, including bombardment, is permitted.
  D) Define a noncombatant.
  E) Describe how members of 8th SOF(N) may be court-martialed for violating these rules.

3. Combat Techniques / Unit Formations (Personnel, & Vehicles)
  A) Bounding,
  B) Movement under Direct Fire
  C) Reactions under Indirect Fire
  D) Select a Temporary Firing Position
  E) Cordon
  F) Threat analysis

4. Radio Communications / Etiquette
  A) Short Range
  B) Long Range

5. Weapons Qualifications:
  A) Pistol - Iron Sights 100m/300m 18/20
  B) LMG - Iron Sights 100m/300m 8/10 - 200m/400m 8/10
  C) Rifle - Iron Sights 58/60
  D) M203 - 7
  E) RPG (Non Guided) - 4

6. Navigation
  A) TOPO Map Basics, Contour Lines/interval, Latitude and Longitude
  B) Compass, Bearing, and Degree Reading (Azimuth)
  C) GPS Ctrl+m
  D) Setting waypoints
